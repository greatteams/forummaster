<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
             <div class="col-md-2">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation"><a href="<?= base_url('admin') ?>">Home</a></li>
				<li role="presentation" ><a href="<?= base_url('admin/users') ?>">Back to Users</a></li>
				<li role="presentation" class="active"><a href="<?= base_url('admin/forums_and_topics') ?>">Forums & topics</a></li>
				<li role="presentation"><a href="<?= base_url('admin/options') ?>">Options</a></li>
				<li role="presentation"><a href="<?= base_url('admin/emails') ?>">Emails</a></li>
			</ul>
		</div>
		<div class="col-md-10">
			<div class="page-header">
				<h1>Forum creation success!</h1>
			</div>
			<p>Your new forum has been created succesfuly.</p>
		</div>
	</div><!-- .row -->
</div><!-- .container -->