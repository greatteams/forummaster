<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
            <div class="col-md-2">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation"><a href="<?= base_url('admin') ?>">Home</a></li>
				<li role="presentation" ><a href="<?= base_url('admin/users') ?>">Back to Users</a></li>
				<li role="presentation" class="active"><a href="<?= base_url('admin/forums_and_topics') ?>">Forums & topics</a></li>
				<li role="presentation"><a href="<?= base_url('admin/options') ?>">Options</a></li>
				<li role="presentation"><a href="<?= base_url('admin/emails') ?>">Emails</a></li>
			</ul>
		</div>
		<div class="col-md-10">
			<?= $breadcrumb ?>
		</div>
		<div class="col-md-10">
			<div class="page-header">
				<h1>Create a new forum</h1>
			</div>
		</div>
		<?php if ($login_as_admin_needed) : ?>
			<div class="col-md-10">
				<div class="alert alert-danger" role="alert">
					<p>You need to be logged in as an administrator to create a new forum!</p>
					<p>Please <a href="<?= base_url('login') ?>">login</a>.</p>
				</div>
			</div>
		<?php else : ?>
			<?php if (validation_errors()) : ?>
				<div class="col-md-10">
					<div class="alert alert-danger" role="alert">
						<?= validation_errors() ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if (isset($error)) : ?>
				<div class="col-md-10">
					<div class="alert alert-danger" role="alert">
						<?= $error ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-md-10">
				<?= form_open() ?>
					<div class="form-group">
						<label for="title">Title</label>
						<input type="text" class="form-control" id="title" name="title" placeholder="Enter a forum title">
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea rows="6" class="form-control" id="description" name="description" placeholder="Enter short description for the new forum (max 80 characters)"></textarea>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-default" value="Create forum">
					</div>
				</form>
			</div>
		<?php endif; ?>
	</div><!-- .row -->
</div><!-- .container -->