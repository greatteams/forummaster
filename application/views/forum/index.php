<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
             <div class="col-md-2">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation"><a href="<?= base_url('admin') ?>">Home</a></li>
				<li role="presentation" ><a href="<?= base_url('admin/users') ?>">Back to Users</a></li>
				<li role="presentation" class="active"><a href="<?= base_url('admin/forums_and_topics') ?>">Forums & topics</a></li>
				<li role="presentation"><a href="<?= base_url('admin/options') ?>">Options</a></li>
				<li role="presentation"><a href="<?= base_url('admin/emails') ?>">Emails</a></li>
			</ul>
		</div>
		
		<div class="col-md-10">
			<div class="page-header">
				<h1>Forums</h1>
			</div>
		</div>
		
		<div class="col-md-10">
			<table class="table table-striped table-condensed table-hover">
				<caption></caption>
				<thead>
					<tr>
						<th>Forums</th>
						<th>Topics</th>
						<th>Posts</th>
						<th class="hidden-xs">Latest topic</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($forums) : ?>
						<?php foreach ($forums as $forum) : ?>
							<tr>
								<td>
									<p>
										<a href="<?= base_url($forum->slug) ?>"><?= $forum->title ?></a><br>
										<small><?= $forum->description ?></small>
									</p>
								</td>
								<td>
									<p>
										<small><?= $forum->count_topics ?></small>
									</p>
								</td>
								<td>
									<p>
										<small><?= $forum->count_posts ?></small>
									</p>
								</td>
								<td class="hidden-xs">
									<?php if ($forum->latest_topic->title !== null) : ?>
										<p>
											<small><a href="<?= base_url($forum->latest_topic->permalink) ?>"><?= $forum->latest_topic->title ?></a><br>by <a href="<?= base_url('user/' . $forum->latest_topic->author) ?>"><?= $forum->latest_topic->author ?></a>, <?= $forum->latest_topic->created_at ?></small>
										</p>
									<?php else : ?>
										<p>
											<small>no topic yet</small>
										</p>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
			
		
		
		<?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === true) : ?>
			<div class="col-md-10">
				<a href="<?= base_url('admin/forums_and_topics') ?>" class="btn btn-default">Create a new forum</a>
			</div>
		<?php endif; ?>
		
	</div><!-- .row -->
        </div>
</div><!-- .container -->

<?php //var_dump($forums); ?>